package org.acm;

import java.util.concurrent.atomic.AtomicInteger;

public class Payment {
    private static AtomicInteger ID_GENERATOR = new AtomicInteger(1000);
    int paymentId;
    String cid;
    String mid;
    int amount;

    public Payment() {

    }

    public Payment(int amount, String cid, String mid) {
        this.paymentId= ID_GENERATOR.getAndIncrement();
        this.cid = cid;
        this.mid = mid;
        this.amount = amount;
    }

    public String getCid() {
        return cid;
    }

    public String getMid() {
        return mid;
    }

    public int getAmount() {
        return amount;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getPaymentId() {
        return paymentId;
    }
}
