package org.acm;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

public class PaymentService {

    WebTarget baseUrl;
    String errorMessage;

    public PaymentService()
    {
        Client client = ClientBuilder.newClient();
        baseUrl = client.target("http://localhost:8080/");
    }


    public boolean pay(int amount, String cid, String mid)
    {
        Payment payment = new Payment();
        payment.setCid(cid);
        payment.setMid(mid);
        payment.setAmount(amount);

        Response response = baseUrl.path("payments").request().post(Entity.entity(payment, MediaType.APPLICATION_JSON));
        if(response.getStatus()==400)
        {
            errorMessage = response.readEntity(String.class);
            return false;
        }
        return true;
    }

    public List<Payment> listPayments(){

        return baseUrl.path("payments").request()
                .get(new GenericType<List<Payment>>() {
                });
    }

}
