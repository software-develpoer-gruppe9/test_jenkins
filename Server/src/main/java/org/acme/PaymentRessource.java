package org.acme;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;


@Path("/payments")
public class PaymentRessource {
    PaymentService dtuPayment = PaymentService.Instance;


    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response registerPayment(Payment payment) throws Exception{
        try {
            int id = dtuPayment.registerPayment(payment);
            return Response.created(new URI("payments/"+id)).build();
        }catch (Exception exception){
            return Response.status(400).entity(exception.getMessage()).type("text/plain").build();
        }
    }


//    @POST
//    @Path("pay")
//    @Produces(MediaType.APPLICATION_JSON)
//    @Consumes(MediaType.APPLICATION_JSON)
//    public boolean pay(Payment payment) throws Exception{
//        return dtuPayment.registerPayment(payment);
//    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Payment> paymentList(@QueryParam("amount") int amount, @QueryParam("cid") String cid, @QueryParam("mid") String mid) {
        return dtuPayment.getPayment(amount, cid, mid);
    }
}